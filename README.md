# Faircoinbot

[@Faircoinbot](https://t.me/Faircoinbot "@Faircoinbot") - enjoy it!

This is a telegram bot to control balance of your Faircoin wallets, you can add your faircoin addresses (wallets)
 and control your balance of Faircoins on it, also the bot is going to check balance of your
 wallets and is going to keep you informed about all changes on it.


* Had been used [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot "python-telegram-bot API Library GitHub Repository") API Library,
you can use *start_polling* or *webhook* updates methods to recieve the messages (see faircoinmonitor_bot.py code and pyTelegramBotAPI Library manual)

* Code forked from https://github.com/lytves/ETHdroidBot (MIT license)

* [MongoBD database](https://github.com/mongodb/mongo) and [PyMongo](https://github.com/mongodb/mongo-python-driver "PyMongo") - the Python driver for MongoDB had been used
to store user's wallets and balances

* “Powered by [chain.fair.to](https://chain.fair.to "Faircoin Chain explorer")” for view faircoin addresses balances.

### Install:

You need to install:

+ python (tested on 3.5+ version)

+ Install dependencies with:

`$ pip install -r requeriments.txt`

+ mongod - The database server

But in any case you must read authentic modules documentation to use it in your own operating system
 and environment

### Settings:

Bot Settings are in the file **fairdroid/config.py:**

* admin telegram alias *YOUR_TELEGRAM_ALIAS*
* some your API urls settings
* put your MongoDB settings: *MONGO_DB_NAME* and *MONGO_DB_COLLECTION*

### Run:

`$ python faircoinmonitor_bot.py`

- you must use your python correct command depend of your python version and install path

---

Screenshot of the working bot:

![faircoinmonitor_bot](https://gitlab.com/komun.org/faircoin-monitor-bot/raw/master/images/capture.png "faircoinmonitor_bot")
