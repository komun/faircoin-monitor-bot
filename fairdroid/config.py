# put your bot's token here
TOKEN_BOT = '6821331572:AAFbNyudGdHXuYZJ-NaN0PJtUl_jegRC9Is'
YOUR_TELEGRAM_ALIAS = 'youralias'

#
# # Ethplorer.io API
# FAIRPLORER_API_KEY = 'freekey'
# FAIRPLORER_API_URL = 'https://api.ethplorer.io/getAddressInfo/{}?apiKey=' + FAIRPLORER_API_KEY
#
#
# # FAIRERSCAN.io API
# FAIRERSCAN_API_KEY = 'put_your_api_token_here'
# FAIRERSCAN_API_URL = 'https://api.etherscan.io/api?module=stats&action=ethprice&apikey=' + FAIRERSCAN_API_KEY
#
#
# # CryptoCompare API
# CRYPTOCOMPARE_API_URL = 'https://min-api.cryptocompare.com/data/price?fsym=FAIR&tsyms=BTC,USD,EUR'
CHAIN_FAIRTO_URL = 'https://chain.fair.to/address?address={}'

# number of Faircoin wallets user can to add
NUMBER_WALLETS = 5
# Faircoin address format
LENGTH_WALLET_ADDRESS = 34
# to split long send_message (Telegram restrictions 4096 UTF8 characters,
# to use parse_mode="Markdown" method we must increase it for 1600-1700 characters)
MAX_MESSAGE_LENGTH = 1600


# put your DB name and collection name here
MONGO_DB_NAME = 'faircoin_monitor_bot'
MONGO_DB_COLLECTION = 'wallets'
