from fairdroid.config import NUMBER_WALLETS

# Russian language ('ru' and 'ru-RU')
RUSSIAN = {'TXT_USE_START_BUTTON': '🚀 Используй команду /start для начала работы с ботом!',
           'MENU_ADD_FAIR_WALLET': 'Добавить кошелёк',
           'MENU_DEL_FAIR_WALLET': 'Удалить кошелёк',
           'MENU_CHECK_ALL_BALANCE': 'Мой баланс',
           'MENU_BOT_OPTIONS': 'Настройки',
           'MENU_SHARE_BOT': 'Поделиться ботом',
           'MENU_FEEDBACK': 'Обратная связь',
           'TXT_USE_KEYBOARD': '📱 Используй клавиатуру бота, пожалуйста',
           'TXT_SHARE_BOT': 'Отправь ссылку на этого бота своим друзьям! 👇',
           'TXT_FEEDBACK': 'Напишите ваше сообщение для администратора:',
           'MENU_GO_BACK': 'Назад',
           'TXT_ERROR': '😓 К сожалению не могу отобразить информацию сейчас. Пожалуйста, попробуй позже!',
           'TXT_ADD_FAIR_NAME_WALLETS_FULL': '🚫 Превышен лимит на ' + str(NUMBER_WALLETS) + ' *FairCoin кошельков*',
           'TXT_ADD_FAIR_ADDRESS_WALLET': '➕ Введи адрес нового *FairCoin кошелька* f...',
           'TXT_ADD_FAIR_ADDRESS_WALLET_WRONG': '🚩 Неправильный *FairCoin адрес*, попробуй другой f...',
           'TXT_ADD_FAIR_ADDRESS_WALLET_EXISTS': '📌 Этот *FairCoin адрес* уже был добавлен ранее',
           'TXT_ADD_FAIR_ADDRESS_WALLET_ADDED': '*✅ FairCoin адрес* успешно добавлен!\n',
           'LINK_SHARE_BOT': 'Поделиться ботом',
           'LINK_TEXT_SHARE_BOT': 'Всегда будешь в курсе баланса Эфириума и ERC токенов на твоих кошельках! 👍',
           'TXT_FAIR_ADDRESS': '📌 *FairCoin адрес:* ',
           'TXT_FAIR_TOKENS': '📎 *Tокены ERC*:',
           'TXT_FAIR_TOKENS_EMPTY': '📎 *Нет ERC токенов*',
           'TXT_DEL_FAIR_WALLET': '❌ Выбери *FairCoin кошелёк* для удаления:',
           'TXT_NO_FAIR_WALLET': 'ℹ Нет ни одного *FairCoin кошелька*!',
           'TXT_WALLET_UPDATES': '👉👉👉 Изменение баланса:',
           'TXT_PRICE': 'Курс FAIR: ',
           'TXT_START_MSG': '🤖 Привет, я твой fairdroid Bot! Я помогу тебе быть в курсе '
                            'актуального баланса Эфириум и ERC токенов на твоих кошельках.',
           'TXT_GENERAL_MSG': 'Наблюдаются проблемы у API сервиса FairCoin. Временно не работает запрос о'\
                            + ' балансе кошелька.'}

# Spanish language ('es' and 'es-ES')
SPANISH = {'TXT_USE_START_BUTTON': '🚀 ¡Utiliza el comando /start para empezar usar el bot!',
           'MENU_ADD_FAIR_WALLET': 'Añadir dirección',
           'MENU_DEL_FAIR_WALLET': 'Quitar dirección',
           'MENU_CHECK_ALL_BALANCE': 'Mi saldo',
           'MENU_BOT_OPTIONS': 'Ajustes',
           'MENU_SHARE_BOT': 'Compartir el bot',
           'MENU_FEEDBACK': 'Feedback',
           'TXT_USE_KEYBOARD': '📱 Usa el teclado del bot, por favor',
           'TXT_SHARE_BOT': '¡Comparte este bot con tus amigos! 👇',
           'TXT_FEEDBACK': 'Escribe tu mensaje para el administrador:',
           'MENU_GO_BACK': 'Atrás',
           'TXT_ERROR': '😓 Lo siento, ahora no puedo mostarte información. ¡Pruébalo más tarde por favor!',
           'TXT_ADD_FAIR_NAME_WALLETS_FULL': '🚫 Está permitido añadir solo ' + str(NUMBER_WALLETS) + ' *direcciones de FairCoin*',
           'TXT_ADD_FAIR_ADDRESS_WALLET': '➕ Entra la dirección de tu *cartera de FairCoin*: fXXXXXX...',
           'TXT_ADD_FAIR_ADDRESS_WALLET_WRONG': '🚩 Esta *direccíon de FairCoin* no está correcta, pruebalo de nuevo: fXXXXXX...',
           'TXT_ADD_FAIR_ADDRESS_WALLET_EXISTS': '📌 Esta *direccíon de FairCoin* ya está en tu lista',
           'TXT_ADD_FAIR_ADDRESS_WALLET_ADDED': '✅ ¡*La direccíon de FairCoin* añadida con éxito!\n',
           'LINK_SHARE_BOT': 'Compartir el bot',
           'LINK_TEXT_SHARE_BOT': '¡Recibe notificaiones de tu saldo en tus direcciones FairCoin! 👍',
           'TXT_FAIR_ADDRESS': '📌 *Dirección FairCoin:* ',
           'TXT_FAIR_TOKENS': '📎 *Tokens ERC:*',
           'TXT_FAIR_TOKENS_EMPTY': '📎 *Aún no hay ERC tokens*',
           'TXT_DEL_FAIR_WALLET': '❌ Elige una *dirección de FairCoin* para borrar:',
           'TXT_NO_FAIR_WALLET': 'ℹ ¡No tienes ninguna *dirección de FairCoin*!',
           'TXT_WALLET_UPDATES': '👉👉👉 Saldo se ha cambiado:',
           'TXT_PRICE': 'Precio FAIR: ',
           'TXT_START_MSG': '🤖 ¡Hola! Soy tu FairCoin Bot, te ayudo para que siempre sepas el saldo de FairCoin de tus direcciones.',
           'TXT_GENERAL_MSG': 'El servicio API de FairCoin ahora tiene algúnos problemas. La información'\
                            + ' sobre saldo no está disponible temporalmente.'}

# English language (by default)
ENGLISH = {'TXT_USE_START_BUTTON': '🚀 Use the command /start to bot\'s work!',
           'MENU_ADD_FAIR_WALLET': 'Add address',
           'MENU_DEL_FAIR_WALLET': 'Delete address',
           'MENU_CHECK_ALL_BALANCE': 'My balance',
           'MENU_BOT_OPTIONS': 'Options',
           'MENU_SHARE_BOT': 'Share the bot',
           'MENU_FEEDBACK': 'Feedback',
           'TXT_USE_KEYBOARD': '📱 Use the bot keyboard, please',
           'TXT_SHARE_BOT': 'Share this bot with your friends! 👇',
           'TXT_FEEDBACK': 'Write your message for the admin:',
           'MENU_GO_BACK': 'Back',
           'TXT_ERROR': '😓 Sorry, I can\'t show you information now. Please, try again later!',
           'TXT_ADD_FAIR_NAME_WALLETS_FULL': '🚫 Allow limit is ' + str(NUMBER_WALLETS) + ' *FairCoin addresses*',
           'TXT_ADD_FAIR_ADDRESS_WALLET': '➕ Enter the address of your *FairCoin wallet*: fXXXXXX...',
           'TXT_ADD_FAIR_ADDRESS_WALLET_WRONG': '🚩 This *FairCoin address* is not correct, try another one: fXXXXXX...',
           'TXT_ADD_FAIR_ADDRESS_WALLET_EXISTS': '📌 This *FairCoin address* has been already added earlier',
           'TXT_ADD_FAIR_ADDRESS_WALLET_ADDED': '✅ *The FairCoin address* has been added successfully!\n',
           'LINK_SHARE_BOT': 'Share the bot',
           'LINK_TEXT_SHARE_BOT': 'Stay informed of the balance of FairCoin addresses! 👍',
           'TXT_FAIR_ADDRESS': '📌 *FairCoin address:* ',
           'TXT_FAIR_TOKENS': '📎 *Tokens ERC*:',
           'TXT_FAIR_TOKENS_EMPTY': '📎 *There is not any token ERC yet*',
           'TXT_DEL_FAIR_WALLET': '❌ Choose a *FairCoin address* for delete:',
           'TXT_NO_FAIR_WALLET': 'ℹ There is no *FairCoin addresses*!',
           'TXT_WALLET_UPDATES': '👉👉👉 Balance changed:',
           'TXT_PRICE': 'Price FAIR: ',
           'TXT_START_MSG': '🤖 Hello! I am your FairCoin Bot, I will help you '
                            'to control the balance of your FairCoin addresses.',
           'TXT_GENERAL_MSG': 'FairCoin API service has a some working problems. Information about'\
                                + ' address balance temporarily doesn\'t accessible '}
